package id.ac.ub.papb.async2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    final static String source = "https://medium.com/feed/tag/programming";
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = findViewById(R.id.tv1);
        AsyncExample task=new AsyncExample(tv1);
        task.execute();
    }

    class AsyncExample extends AsyncTask<String, String, String> {
        TextView tv1;

        public AsyncExample(TextView tv1) {
            this.tv1 = tv1;
        }

        @Override
        protected String doInBackground(String... strings) {
            RssParser parser = new RssParser();
            try {
                String xml = parser.loadRssFromUrl(source);
                return xml;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tv1.setText(s);
        }
    }


}